package com.googya;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

public class WebSocketServer {
    public static void startUp() throws Exception {
        // 监听端口的线程组
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        // 处理连接的数据读写的线程组
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        //
        ServerBootstrap serverBootstrap = new ServerBootstrap();

        try {
            serverBootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel socketChannel) throws Exception {
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            pipeline.addLast("logger", new LoggingHandler(LogLevel.INFO));

                            pipeline.addLast("http-codec", new HttpServerCodec());

                            pipeline.addLast("aggregator", new HttpObjectAggregator(65536));

                            pipeline.addLast("http-chuncked", new ChunkedWriteHandler());

                            pipeline.addLast("handler", new WebSocketServerHandler());
                        }
                    })

                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new ChannelInitializer<NioServerSocketChannel>() {

                        @Override
                        public void initChannel(NioServerSocketChannel nioServerSocketChannel) throws Exception {
                            System.out.println("WebSocket 服务端启动中。。。");
                        }
                    });
            ChannelFuture f = serverBootstrap.bind(8806).sync();
            f.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdown();
            workerGroup.shutdown();
        }
    }

    public static void main(String[] args) throws Exception {
        startUp();
    }
}
